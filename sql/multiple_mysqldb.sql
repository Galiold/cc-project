CREATE DATABASE IF NOT EXISTS restaurant_customer;
CREATE DATABASE IF NOT EXISTS restaurant_panel;

GRANT ALL ON restaurant_customer.* TO 'root'@'localhost';
GRANT ALL ON restaurant_panel.* TO 'root'@'localhost';
